#!/bin/bash
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
export PATH

#######################################################
#                   NGINXAUTO v1.4.3                  #
# Written by 2298@otoinstall.com and Junker@vbfk.com  #
#                 Based on LNMP Script                #
#######################################################

if [ $(id -u) != "0" ]; then
    echo "Error: You must be root to run this script, please use root to install this script"
    exit 0
fi
clear

echo "================================================================================"
echo "           NGINX - PHP - MYSQL AUTO INSTALLER v1.4.3 FOR CENTOS 5 & 6"            
echo "================================================================================"
echo "                    2298@otoinstall.com & Junker@vbfk.com"
echo "                      For more information please visit:"
echo "             http://otoinstall.com/nginx-auto-installer-php-mysql"
echo "================================================================================"

arch=`uname -i`
proc=`cat /proc/cpuinfo | grep -c processor`
centos=`cat /etc/redhat-release`
myhome=`hostname`
mkdir -p /tmp/pkgs

if [ "$arch" == "x86_64" ]; then
	library="lib64"
else
    library="lib"
fi

#Set MySQL root password
SAKSESBUNG=0
COBALAGI=1
while [ $SAKSESBUNG -eq "0" ]; do
	read -p "Please input your preferred mysql root password: " mysqlrootpwd
	if [ "$mysqlrootpwd" = "" ]; then
		if [ $COBALAGI -lt 3 ]; then
			echo ""
			echo "You're not typing, it is still empty!"
			echo ""
			COBALAGI=`expr $COBALAGI + 1`
		else
			echo ""
			echo "You might want to try it again later."
			echo ""
			exit 0
		fi
	else
		mysqlrootpwd="$mysqlrootpwd"
		SAKSESBUNG=1
	fi
done
echo ""

#Set FTP/SFTP password
SAKSESBUNG=0
COBALAGI=1
while [ $SAKSESBUNG -eq "0" ]; do
	read -p "Please input your preferred ftp/sftp password: " ftppwd
	if [ "$ftppwd" = "" ]; then
		if [ $COBALAGI -lt 3 ]; then
			echo ""
			echo "You're not typing, it is still empty!"
			echo ""
			COBALAGI=`expr $COBALAGI + 1`
		else
			echo ""
			echo "You might want to try it again later."
			echo ""
			exit 0
		fi
	else
		ftppwd="$ftppwd"
		SAKSESBUNG=1
	fi
done
echo ""

#Set phpMyAdmin directory name
SAKSESBUNG=0
COBALAGI=1
while [ $SAKSESBUNG -eq "0" ]; do
	read -p "Please input your preferred phpmyadmin directory name: " phpmyadmin
	if [ "$phpmyadmin" = "" ]; then
		if [ $COBALAGI -lt 3 ]; then
			echo ""
			echo "You're not typing, it is still empty!"
			echo ""
			COBALAGI=`expr $COBALAGI + 1`
		else
			echo ""
			echo "You might want to try it again later."
			echo ""
			exit 0
		fi
	else
		phpmyadmin="$phpmyadmin"
		SAKSESBUNG=1
	fi
done
echo ""

#Confirm PHP Opcode Cacher

echo "Type 'apc' for APC PHP"
echo "Type 'xcache' for XCache"
while read -p "Please choose your preferred PHP Opcode Cacher: " phpcacher
	do case $phpcacher in
	apc) [ "phpcacher" = "$phpcacher" ]; break;;
	xcache) [ "phpcacher" = "$phpcacher" ]; break;;
	*) echo "";
	   echo "Please type the right choice!";;
	esac
done
echo ""


#Confirm Memcached
while read -p "Would you like to install Memcached?[Y/n] " memcached
	do case $memcached in
	Y) [ "memcached" = "$memcached" ]; break;;
	y) [ "memcached" = "$memcached" ]; break;;
	N) [ "memcached" = "$memcached" ]; break;;
	n) [ "memcached" = "$memcached" ]; break;;
	*) echo "";
	   echo "Please type the right choice!";;
	esac
done
echo ""

#Confirm IMAGICK
while read -p "Would you like to install Image Magick?[Y/n] " imagick
	do case $imagick in
	Y) [ "imagick" = "$imagick" ]; break;;
	y) [ "imagick" = "$imagick" ]; break;;
	N) [ "imagick" = "$imagick" ]; break;;
	n) [ "imagick" = "$imagick" ]; break;;
	*) echo "";
	   echo "Please type the right choice!";;
	esac
done
echo ""

#Confirm FFMPEG

while read -p "Would you like to install FFMPEG?[Y/n] " ffmpeg
	do case $ffmpeg in
	Y) [ "ffmpeg" = "$ffmpeg" ]; break;;
	y) [ "ffmpeg" = "$ffmpeg" ]; break;;
	N) [ "ffmpeg" = "$ffmpeg" ]; break;;
	n) [ "ffmpeg" = "$ffmpeg" ]; break;;
	*) echo "";
	   echo "Please type the right choice!";;
	esac
done
echo ""


#Confirm PureFTPD
while read -p "Would you like to install PureFTPD?[Y/n] " pureftpd
	do case $pureftpd in
	Y) [ "pureftpd" = "$pureftpd" ]; break;;
	y) [ "pureftpd" = "$pureftpd" ]; break;;
	N) [ "pureftpd" = "$pureftpd" ]; break;;
	n) [ "pureftpd" = "$pureftpd" ]; break;;
	*) echo "";
	   echo "Please type the right choice!";;
	esac
done

	get_char()
	{
	SAVEDSTTY=`stty -g`
	stty -echo
	stty cbreak
	dd if=/dev/tty bs=1 count=1 2> /dev/null
	stty -raw
	stty echo
	stty $SAVEDSTTY
	}
	echo ""
	echo "Press any key to start the installation..."
	char=`get_char`

echo ""
echo "================================DISABLING SELINUX==============================="
echo ""
if [ -s /etc/selinux/config ]; then
sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
fi
/usr/sbin/setenforce 0


wget -q -O - http://www.atomicorp.com/installers/atomic |sh
rpm -Uvh http://download.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm
rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-6.rpm

ARCH=$(uname -i)
SYSTEM=$(cat /etc/issue | awk -F' ' '{if(NR==1) print $1}')
VERSION=$(cat /etc/issue | awk -F'[ |.]' '{if(NR==1) print $3}')

cat > /etc/yum.repos.d/nginx.repo <<EOF
[nginx]
name=nginx repo
baseurl=http://nginx.org/packages/centos/${VERSION}/${ARCH}/
gpgcheck=0
enabled=1
EOF

yum -y update;

echo ""
echo "=============================INSTALLING DEPENDENCIES============================"
echo ""
yum -y --enablerepo=remi,remi-test install gcc gcc-c++ make automake patch bc nc GeoIP GeoIP-devel GeoIP-data fontconfig fontconfig-devel gd gd-devel pam pam-devel openldap openldap-devel libXpm libXpm-devel libtool libaio libaio-devel libtool-ltdl libtool-ltdl-devel libmcrypt libmcrypt-devel mhash mhash-devel unixODBC unixODBC-devel zip unzip nano perl perl-devel openssl openssl-devel pcre pcre-devel bzip2 bzip2-devel libjpeg libjpeg-devel libpng libpng-devel libtiff libtiff-devel freetype freetype-devel gmp gmp-devel aspell aspell-devel gettext krb5-devel tetex-latex xz icu libicu-devel libXext-devel libXfixes-devel git-core nano
yum -y update

yum -y --enablerepo=remi,remi-test install gnupg curl curl-devel libidn libidn-devel libssh2 libssh2-devel zlib zlib-devel perl-ExtUtils-Embed

/etc/init.d/mysqld stop
/etc/init.d/httpd stop

yum -y remove httpd php mysql

echo "set nowrap" >>/etc/nanorc
cat <<EOF >>/etc/profile.d/nano.sh
export VISUAL="nano"
export EDITOR="nano"
EOF

yum -y --enablerepo=remi,remi-test install wget libxml2 libxml2-devel libxslt libxslt-devel yasm libvpx zlib t1lib libc-client libc-client-devel libtidy libtidy-devel tidy perl-libwww-perl php-mcrypt* uw-imap uw-imap-utils

echo ""
echo "========================DEPENDENCIES INSTALLATION COMPLETED====================="
echo ""

echo ""
echo "=================================INSTALLING NGINX==============================="
echo ""

yum -y --enablerepo=remi,remi-test install nginx

if [ -d /etc/nginx/conf.d ]
then
    mv /etc/nginx/conf.d /etc/nginx/sites-available
else
    mkdir -p /etc/nginx/sites-available
fi

mkdir -p /etc/nginx/sites-enabled

chkconfig --add nginx
chkconfig nginx on

mkdir -p /etc/nginx/sslconf
cd /etc/nginx/sslconf
openssl genrsa -out server.key 1024
#C=Country S=State L=Locality O=Organization OU=Organization Unit CN=Common Name
openssl req -new -key server.key -out server.csr -subj "/C=ab/ST=cd/L=ef/CN=ghij"
mv server.key server.key.org
openssl rsa -in server.key.org -out server.key
openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt

echo nginx:$ftppwd|chpasswd

echo ""
echo "          -----------------CREATING MASTER VIRTUAL HOST---------------          "
echo ""

if [ -f /etc/nginx/nginx.conf ]
then
    rm /etc/nginx/nginx.conf;
fi

cat >/etc/nginx/nginx.conf<<EOF

user              nginx;
worker_processes  $proc;

error_log		/var/log/nginx/error.log;
#error_log  logs/error.log  notice;
#error_log  logs/error.log  info;

pid	/var/run/nginx.pid;


events {
	worker_connections	1024;
}


http {
	include		/etc/nginx/mime.types;
	default_type	application/octet-stream;
	client_max_body_size	10M;
	client_body_buffer_size 128k;
	server_names_hash_bucket_size  64;

	log_format  main  '$remote_addr | $remote_user [$time_local] | "$request"'
			  ' | $status | $body_bytes_sent bytes | "$http_referer"'
			  ' | "$http_user_agent" | "$http_x_forwarded_for"';

	access_log  /var/log/nginx/access.log  main; 

	sendfile        on;
	tcp_nopush     on;
	keepalive_timeout  65;
	tcp_nodelay on;

  gzip  on;
    
	# Load virtual host configuration files.
  include /etc/nginx/sites-enabled/*;
}

EOF

echo ""
echo "===========================NGINX INSTALLATION COMPLETED========================="
echo ""

echo ""
echo "================================INSTALLING MYSQL================================"
echo ""

yum -y --enablerepo=remi,remi-test  install mysql-server
chkconfig --add mysqld
chkconfig --levels 235 mysqld on

/etc/init.d/mysqld start

echo "sleep 300 seconds"
echo ""
echo "          ------------------------SECURING MYSQL----------------------          "
echo ""

/usr/bin/mysqladmin -u root password $mysqlrootpwd
echo 'DROP DATABASE 'test';' | /usr/bin/mysql -u root -p$mysqlrootpwd
echo "SET PASSWORD FOR 'root'@'127.0.0.1' = PASSWORD('$mysqlrootpwd');" | /usr/bin/mysql -u root -p$mysqlrootpwd
echo "SET PASSWORD FOR 'root'@'`hostname`' = PASSWORD('$mysqlrootpwd');" | /usr/bin/mysql -u root -p$mysqlrootpwd
echo "DELETE FROM mysql.user WHERE User = '';" | /usr/bin/mysql -u root -p$mysqlrootpwd
echo "DROP USER ''@'%';" | /usr/bin/mysql -u root -p$mysqlrootpwd
echo "DROP USER 'root'@'::1';" | /usr/bin/mysql -u root -p$mysqlrootpwd
/etc/init.d/mysqld stop
rm -rf /var/lib/mysql/mysql/data/test

echo ""
echo "===========================MYSQL INSTALLATION COMPLETED========================="
echo ""

echo ""
echo "==================================INSTALLING PHP================================"
echo ""

yum -y --enablerepo=remi,remi-test install php-common php-fpm

echo ""
echo "          ----------------------INSTALLING MODULES--------------------          "
echo ""

if [ $phpcacher = "xcache" ]; then
  yum -y  --enablerepo=remi,remi-test install php-xcache
elif [ $phpcacher = "apc" ]; then
  yum -y --enablerepo=remi,remxi-test install php-pear php-devel pcre-devel make php-pecl-apc
fi

if [ "d$imagick" = "d" ] || [ `expr "$imagick" : '[yY]'` -gt 0 ]; then
	yum -y --enablerepo=remi,remi-test install ImageMagick
	yum -y --enablerepo=remi,remi-test install ImageMagick-devel
	pecl install imagick
fi

if [ "d$ffmpeg" = "d" ] || [ `expr "$ffmpeg" : '[yY]'` -gt 0 ]; then

	yum -y install yum-protectbase;
	
	
	cat >/etc/yum.repos.d/ffmpeg.repo<<EOF
	[ffmpeg]
   name=FFmpeg RPM Repository for Red Hat Enterprise Linux
   baseurl=http://apt.sw.be/redhat/el6/en/x86_64/dag/
   gpgcheck=1
   enabled=1
EOF

  cp -r /etc/ld.so.conf  /usr/local/lib/
  ldconfig -v
  rpm -Uvh http://apt.sw.be/redhat/el6/en/x86_64/rpmforge/RPMS/rpmforge-release-0.5.3-1.el6.rf.x86_64.rpm
  yum -y update
  yum -y install ffmpeg ffmpeg-devel
fi

if [ "d$memcached" = "d" ] || [ `expr "$memcached" : '[yY]'` -gt 0 ]; then
	yum -y --enablerepo=remi,remi-test install memcached php-pecl-memcached
	chkconfig --levels 235 memcached on
	/etc/init.d/memcached start
fi


echo ""
echo "============================PHP INSTALLATION COMPLETED=========================="
echo ""

echo ""
echo "===============================INSTALLING PHPMYADMIN============================"
echo ""

/etc/init.d/mysqld start
wget -O /tmp/pkgs/phpmyadmin.zip http://pkgs.otoinstall.com/phpmyadmin.zip
unzip -d /tmp/pkgs /tmp/pkgs/phpmyadmin.zip
/usr/bin/mysql -u root -p$mysqlrootpwd < /tmp/pkgs/phpmyadmin/examples/create_tables.sql
/usr/bin/mysql -u root -p$mysqlrootpwd < /tmp/pkgs/phpmyadmin/examples/upgrade_tables_mysql_4_1_2+.sql
echo "CREATE USER 'phpmyadmin'@'localhost' IDENTIFIED BY 'h0nGopq0nhes';" | /usr/bin/mysql -u root -p$mysqlrootpwd
echo "GRANT SELECT, INSERT, UPDATE, DELETE ON phpmyadmin.* TO 'phpmyadmin'@'localhost';" | /usr/bin/mysql -u root -p$mysqlrootpwd
echo "FLUSH PRIVILEGES;" | /usr/bin/mysql -u root -p$mysqlrootpwd
mv /tmp/pkgs/phpmyadmin /usr/html/$phpmyadmin

echo ""
echo "================================INSTALLING PUREFTPD============================="
echo ""

if [ "d$pureftpd" = "d" ] || [ `expr "$pureftpd" : '[yY]'` -gt 0 ]; then
	wget -O /tmp/pkgs/pure-ftpd-1.0.36.tar.gz http://pkgs.otoinstall.com/pure-ftpd-1.0.36.tar.gz
	tar -C /tmp/pkgs -zxvf /tmp/pkgs/pure-ftpd-1.0.36.tar.gz
	cd /tmp/pkgs/pure-ftpd-1.0.36
	./configure
	make install-strip
	echo "/usr/local/sbin/pure-ftpd &" >> /etc/rc.d/rc.local
	/usr/local/sbin/pure-ftpd &
fi

echo ""
echo "=====================================CLEANING UP==============================="
echo ""

rm -rf /tmp/pkgs

echo ""
echo "============================STARTING YOUR ROCK MACHINE!!!======================="
echo ""

/etc/init.d/nginx start
/etc/init.d/php-fpm start
/etc/init.d/mysqld restart

echo ""
echo "===============================INSTALLATION COMPLETED==========================="
echo ""

echo "You're suggested to reboot first before start using your box."
echo ""
echo "================================================================================"
echo "               PLEASE SAVE THE FOLLOWING DATA FOR YOUR CREDENTIALS"
echo "================================================================================"
echo "Post Installation:    http://$myhome"
echo ""
echo "FTP/SFTP Hostname:    $myhome"
echo "FTP/SFTP URI:         ftp://www@$myhome"
echo "FTP/SFTP User:        www"
echo "FTP/SFTP Pass:        $ftppwd"
echo "FTP/SFTP Directory:   /home/www"
echo ""
echo "MySQL Root Pass:      $mysqlrootpwd"
echo "phpMyAdmin URL:       https://$myhome/$phpmyadmin"
echo "phpMyAdmin User:      root"
echo "phpMyAdmin Pass:      $mysqlrootpwd"
echo "================================================================================"
echo ""