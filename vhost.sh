#!/bin/bash
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
export PATH

# Check if user is root
if [ $(id -u) != "0" ]; then
    echo "Error: You must be root to run this script, please use root to install this script"
    exit 1
fi

#######################################################
#        ADD & REMOVE VIRTUAL HOST + WORDPRESS        #
# Written by 2298@otoinstall.com and Junker@vbfk.com  #
#######################################################

clear
echo ""
echo "Type 'V' to add virtual host"
echo "Type 'R' to remove your domain"
while read -p "What do you want to do? [V/R]: " whattodo
	do case $whattodo in
	V) [ "whattodo" = "$whattodo" ]; break;;
	v) [ "whattodo" = "$whattodo" ]; break;;
	R) [ "whattodo" = "$whattodo" ]; break;;
	r) [ "whattodo" = "$whattodo" ]; break;;
	*) echo "";
	   echo "Please type the right choice!";;
	esac
done
echo ""

if [ "d$whattodo" = "d" ] || [ `expr "$whattodo" : '[vV]'` -gt 0 ]; then
clear
echo "================================================================================"
echo "                      ADD VIRTUAL HOST NGINXAUTO v1.4.3"                           
echo "================================================================================"
echo "                    2298@otoinstall.com & Junker@vbfk.com"
echo "                       For more information please visit"
echo "             http://otoinstall.com/nginx-auto-installer-php-mysql"
echo "================================================================================"

#Confirm WordPress
echo "Type 'O' to add virtual host"
echo "Type 'W' to add virtual host plus WordPress"
while read -p "Please type your choice [O/W]: " wpyesno
	do case $wpyesno in
	O) [ "wpyesno" = "$wpyesno" ]; break;;
	o) [ "wpyesno" = "$wpyesno" ]; break;;
	W) [ "wpyesno" = "$wpyesno" ]; break;;
	w) [ "wpyesno" = "$wpyesno" ]; break;;
	*) echo "";
	   echo "Please type the right choice!";;
	esac
done
echo ""

# Directory
SAKSESBUNG=0
COBALAGI=1
while [ $SAKSESBUNG -eq "0" ]; do
read -p "Please input the name of the web drive (ex. d_tdc): " webdrive
	if [ -d /srv/$webdrive ]; then
		webdrive="$webdrive"
		SAKSESBUNG=1	
	else if [ ! -d /srv/$webdrive ] || [ "$webdrive" = "" ]; then
		if [ $COBALAGI -lt 3 ]; then
			echo ""
			echo "You're not typing, it is still empty!"
			echo ""
			COBALAGI=`expr $COBALAGI + 1`
		else
			echo ""
			echo "You might want to try it again later."
			echo ""
			exit 0
		fi
	fi
	fi
done
echo ""

#set domain name
SAKSESBUNG=0
COBALAGI=1
while [ $SAKSESBUNG -eq "0" ]; do
read -p "Please input your desired domain: " domain
	if [ ! -d /srv/$webdrive/$domain ]; then
		domain="$domain"
		SAKSESBUNG=1	
	else if [ ! -d /srv/$webdrive/$domain ] || [ "$domain" = "" ]; then
		if [ $COBALAGI -lt 3 ]; then
			echo ""
			echo "You're not typing, it is still empty!"
			echo ""
			COBALAGI=`expr $COBALAGI + 1`
		else
			echo ""
			echo "You might want to try it again later."
			echo ""
			exit 0
		fi
	else if [ -d /srv/$webdrive/$domain ]; then
		if [ $COBALAGI -lt 3 ]; then
			echo ""
			echo "Your domain is already exist, please add another domain!"
			echo ""
			COBALAGI=`expr $COBALAGI + 1`
		else
			echo ""
			echo "You might want to check your existing domain first."
			echo ""
			exit 0
		fi
	fi
	fi
	fi
done
echo ""

if [ "d$wpyesno" = "d" ] || [ `expr "$wpyesno" : '[wW]'` -gt 0 ]; then
#set mysql root password
SAKSESBUNG=0
COBALAGI=1
AYA=mysql
while [ $SAKSESBUNG -eq "0" ]; do
read -p "Please input your mysql root password: " mysqlrootpwd
CHECK=`/usr/local/mysql/bin/mysql -u root -p$mysqlrootpwd --batch --skip-column-names -e "SHOW DATABASES LIKE 'mysql';"`
	if [ "$AYA" = "$CHECK" ]; then
		mysqlrootpwd="$mysqlrootpwd"
		SAKSESBUNG=1
	else if [ "$mysqlrootpwd" = "" ]; then
			if [ $COBALAGI -lt 3 ]; then
				echo ""
				echo "You're not typing, it is still empty!"
				echo ""
				COBALAGI=`expr $COBALAGI + 1`
			else
				echo ""
				echo "You might want to try it again later."
				echo ""
				exit 0
			fi
	else if [ "$AYA" != "$CHECK" ]; then
			if [ $COBALAGI -lt 3 ]; then
				echo ""
				echo "You have entered the wrong mysql root password, try again!"
				echo ""
				COBALAGI=`expr $COBALAGI + 1`
			else
				echo ""
				echo "You might want to check your mysql root password first."
				echo ""
				exit 0
			fi
	fi
	fi
	fi	
done
echo ""

#set database name
SAKSESBUNG=0
COBALAGI=1
while [ $SAKSESBUNG -eq "0" ]; do
	read -p "Please input your prefered database name: " dbname
	ayaoi=`/usr/local/mysql/bin/mysql -u root -p$mysqlrootpwd --batch --skip-column-names -e "SHOW DATABASES LIKE '$dbname';"`
	if [ "$dbname" = "$ayaoi" ]; then
		if [ $COBALAGI -lt 3 ]; then
			echo ""
			echo "Your preferred database name is exist, try another name!"
			echo ""
			COBALAGI=`expr $COBALAGI + 1`
		else
			echo ""
			echo "You might want to check your existing database in phpmyadmin."
			echo ""
			exit 0
		fi
	else if [ "$dbname" = "" ]; then
		if [ $COBALAGI -lt 3 ]; then
			echo ""
			echo "You're not typing, it is still empty!"
			echo ""
			COBALAGI=`expr $COBALAGI + 1`
		else
			echo ""
			echo "You might want to try it again later."
			echo ""
			exit 0
		fi
	else
		SAKSESBUNG=1
	fi
	fi	
done
echo ""
	
#set database user
SAKSESBUNG=0
COBALAGI=1
while [ $SAKSESBUNG -eq "0" ]; do

OUTFILE=yy.txt
(
/usr/local/mysql/bin/mysql -u root -p$mysqlrootpwd --batch --skip-column-names -e "SELECT user from mysql.user"; << EOF
EOF
) > $OUTFILE

read -p "Please input your prefered database user: " dbuser
if ! grep -q "$dbuser" "$OUTFILE"; then
	dbuser="$dbuser"
	SAKSESBUNG=1
else if [ "$dbuser" = "" ]; then
		if [ $COBALAGI -lt 3 ]; then
			echo ""
			echo "You're not typing, it is still empty!"
			echo ""
			COBALAGI=`expr $COBALAGI + 1`
		else
			echo ""
			echo "You might want to try it again later."
			echo ""
			rm -rf yy.txt
			exit 0
		fi
else if grep -q "$dbuser" "$OUTFILE"; then
		if [ $COBALAGI -lt 3 ]; then
			echo ""
			echo "Please try another name."
			echo ""
			COBALAGI=`expr $COBALAGI + 1`
		else
			echo ""
			echo "You might want to try it again later."
			echo ""
			rm -rf yy.txt
			exit 0
		fi
	fi
fi
fi
done
rm -rf yy.txt
echo ""

#set database password
SAKSESBUNG=0
COBALAGI=1
while [ $SAKSESBUNG -eq "0" ]; do

	read -p "Please input your prefered database pass: " dbuserpass
	if [ "$dbuserpass" = "" ]; then
		if [ $COBALAGI -lt 3 ]; then
			echo ""
			echo "You're not typing, it is still empty!"
			echo ""
			COBALAGI=`expr $COBALAGI + 1`
		else
			echo ""
			echo "You might want to try it again later."
			echo ""
			exit 0
		fi
	else
		SAKSESBUNG=1
	fi
done
fi

	get_char()
	{
	SAVEDSTTY=`stty -g`
	stty -echo
	stty cbreak
	dd if=/dev/tty bs=1 count=1 2> /dev/null
	stty -raw
	stty echo
	stty $SAVEDSTTY
	}
	echo ""
	echo "Press any key to start or CTRL + C to cancel..."
	char=`get_char`

echo ""
echo "=================================CREATING DIRECTORY============================="
echo ""

mkdir -p /srv/$webdrive/$domain/public_html
mkdir -p /srv/$webdrive/$domain/logs

echo ""
echo "          ----------------------CREATING VHOST CONF-------------------          "
echo ""

if [ "d$wpyesno" = "d" ] || [ `expr "$wpyesno" : '[wW]'` -gt 0 ]; then

cat >/etc/nginx/sites-available/$domain<<EOF
server {
  #move the server_name 'www.' to rewrite
  #if you prefer www.domain.com
  server_name  www.$domain;
  rewrite ^/(.*) http://$domain\$1 permanent;
}

server {
  listen 80;
  #add 'www.' prefix if you prefer www.domain.com
  server_name $domain;
  access_log /srv/$webdrive/$domain/logs/access.log;
  error_log /srv/$webdrive/$domain/logs/error.log;

  location / {
    root /srv/$webdrive/$domain/public_html/;
    index index.html index.htm index.php;
	
	include /etc/nginx/conf/wpconf;
  }

  location ~ \.php$ {
    include /etc/nginx/fastcgi_params;
    fastcgi_pass 127.0.0.1:9000;
    fastcgi_index index.php;
    fastcgi_param SCRIPT_FILENAME /srv/$webdrive/$domain/public_html\$fastcgi_script_name;
  }
}
EOF

if [ ! -d /etc/nginx/conf ]; then
mkdir -p /etc/nginx/conf
fi

wget -O /etc/nginx/conf/wpconf http://txt.otoinstall.com/wpconf

if [ ! -d /etc/nginx/ddata ]; then
mkdir -p /etc/nginx/ddata
fi

cat >/etc/nginx/wp/$domain.wp<<EOF
$dbuser $dbname $mysqlrootpwd
EOF

echo ""
echo "================================INSTALLING WORDPRESS============================"
echo ""

echo ""
echo "          -----------------------CREATING DATABASE--------------------          "
echo ""

echo 'CREATE DATABASE '$dbname';' | /usr/local/mysql/bin/mysql -u root -p$mysqlrootpwd
echo "GRANT ALL PRIVILEGES ON $dbname.* TO '$dbuser'@'localhost' IDENTIFIED BY '$dbuserpass';" | /usr/local/mysql/bin/mysql -u root -p$mysqlrootpwd
echo "FLUSH PRIVILEGES;" | /usr/local/mysql/bin/mysql -u root -p$mysqlrootpwd


echo ""
echo "          ---------------------DOWNLOADING WORDPRESS------------------          "
echo ""

wget -O /tmp/wordpress.tar.gz http://wordpress.org/latest.tar.gz
tar -C /srv/$webdrive/$domain/public_html -zxvf /tmp/wordpress.tar.gz
cp -R /srv/$webdrive/$domain/public_html/wordpress/* /srv/$webdrive/$domain/public_html

echo ""
echo "          ---------------------CONFIGURING WP-CONFIG------------------          "
echo ""

cd /srv/$webdrive/$domain/public_html
mv wp-config-sample.php  wp-config.php
sed -i -e 's@database_name_here@'$dbname'@' wp-config.php
sed -i -e 's@username_here@'$dbuser'@' wp-config.php
sed -i -e 's@password_here@'$dbuserpass'@' wp-config.php
if [ $(php -v | grep -v 'with' | grep -v 'Copyright' | awk {'print $2'}) = "5.2.17" ]; then
	sed -i -e 's/localhost/127.0.0.1/g' wp-config.php
fi

echo ""
echo "==========================WORDPRESS INSTALLATION COMPLETED======================"
echo ""

echo ""
echo "=====================================CLEANING UP================================"
echo ""

cd
rm -rf /srv/$webdrive/$domain/public_html/wordpress
rm -rf /srv/$webdrive/$domain/public_html/index.html
rm -rf /srv/$webdrive/$domain/public_html/wp-config-sample.php
rm -rf /tmp/wordpress.tar.gz

echo ""
echo "================================CREATING DOMAIN DATA============================"
echo ""

domaindata=/etc/nginx/ddata/$domain.data
(
echo $dbuser $dbname $mysqlrootpwd<< EOF
EOF
) > $domaindata

else
cat >/etc/nginx/sites-available/$domain<<EOF
server {
  #move the server_name 'www.' to rewrite
  #if you prefer www.domain.com
  server_name  www.$domain;
  rewrite ^/(.*) http://$domain\$1 permanent;
}

server {
  listen 80;
  #add 'www.' prefix if you prefer www.domain.com
  server_name $domain;
  access_log /srv/$webdrive/$domain/logs/access.log;
  error_log /srv/$webdrive/$domain/logs/error.log;

  location / {
    root /srv/$webdrive/$domain/public_html/;
    index index.html index.htm index.php;
  }

  location ~ \.php$ {
    include /etc/nginx/fastcgi_params;
    fastcgi_pass 127.0.0.1:9000;
    fastcgi_index index.php;
    fastcgi_param SCRIPT_FILENAME /srv/$webdrive/$domain/public_html\$fastcgi_script_name;
  }
}
EOF

cat >/srv/$webdrive/$domain/public_html/index.html<<EOF
<html>
<head>
<title>NginX Success Page</title>
</head>
<body>
<center><h1>NginX Works!!!</h1><br>
<p>Created by <a href="http://otoinstall.com">2298</a> &amp; <a href="http://www.vbfk.com">Junker</a></p>
<p>Please take your time to write a comment <a href="http://otoinstall.com/nginx-auto-installer-php-mysql">here</a> if you have any critic, input, or suggestion regarding this script<br />
If you find this script helpul, you can give a feedback by spreading or promoting to your friends or communities.</p></center>
</body>
</html>
EOF
fi

ln -s /etc/nginx/sites-available/$domain /etc/nginx/sites-enabled

echo ""
echo "==================================GIVING PERMISSION============================="
echo ""

chmod 755 /srv/$webdrive/$domain
chmod 755 /srv/$webdrive/$domain/public_html
chmod 755 /srv/$webdrive/$domain/public_html/wp-content
chmod 755 /srv/$webdrive/$domain/public_html/wp-admin
chmod 755 /srv/$webdrive/$domain/public_html/wp-includes
chown -R nginx:nginx /srv/$webdrive/$domain

echo ""
echo "===========================RESTARTING YOUR ROCK MACHINE!!!======================"
echo ""

/etc/init.d/nginx restart
if [ -s /etc/init.d/php-fpm ]; then
/etc/init.d/php-fpm restart
else if [ -s /etc/init.d/php-fastcgi ]; then
/etc/init.d/php-fastcgi restart
fi
fi

echo ""
echo "===========================ADDING VIRTUAL HOST COMPLETED========================"
echo ""

echo ""
echo "================================================================================"
echo "               PLEASE SAVE THE FOLLOWING DATA FOR YOUR CREDENTIALS"
echo "================================================================================"
if [ "d$wpyesno" = "d" ] || [ `expr "$wpyesno" : '[wW]'` -gt 0 ]; then
echo "COMPLETE THE INSTALLATION BY GO TO:"
echo "http://$domain/wp-admin/install.php"
echo ""
fi
echo "URI:              http://$domain"
echo "Domain Directory: /srv/$webdrive/$domain/public_html/"
if [ "d$wpyesno" = "d" ] || [ `expr "$wpyesno" : '[wW]'` -gt 0 ]; then
echo ""
echo "Database Name: $dbname"
echo "Database User: $dbuser"
echo "Database Pass: $dbuserpass"
fi
echo "================================================================================"
else
clear
echo "================================================================================"
echo "This will remove your domain directory, configuration, including wordpress user"
echo "and database if installed"
echo "================================================================================"
#input domain name
SAKSESBUNG=0
COBALAGI=1
while [ $SAKSESBUNG -eq "0" ]; do
echo ""
read -p "Please input your desired domain to be removed: " domain
	if [ ! -d /srv/$webdrive/$domain ]; then
		if [ $COBALAGI -lt 3 ]; then
			echo ""
			echo "Your domain is not exist, please select another domain!"
			echo ""
			COBALAGI=`expr $COBALAGI + 1`
		else
			echo ""
			echo "You might want to check your existing domain first."
			echo ""
			exit 0
		fi
	else if [ ! -d /srv/$webdrive/$domain ] || [ "$domain" = "" ]; then
		if [ $COBALAGI -lt 3 ]; then
			echo ""
			echo "You're not typing, it is still empty!"
			echo ""
			COBALAGI=`expr $COBALAGI + 1`
		else
			echo ""
			echo "You might want to try it again later."
			echo ""
			exit 0
		fi
	else if [ -d /srv/$webdrive/$domain ]; then
		domain="$domain"
		SAKSESBUNG=1	
	fi
	fi
	fi
done

	get_char()
	{
	SAVEDSTTY=`stty -g`
	stty -echo
	stty cbreak
	dd if=/dev/tty bs=1 count=1 2> /dev/null
	stty -raw
	stty echo
	stty $SAVEDSTTY
	}
	echo ""
	echo "Press any key to start or CTRL + C to cancel..."
	char=`get_char`

if [ -d /srv/$webdrive/$domain/public_html/wp-admin ]; then
echo "DROP USER '`awk '{print $1}' /etc/nginx/ddata/$domain.data`'@'localhost';" | /usr/local/mysql/bin/mysql -u root -p`awk '{print $3}' /etc/nginx/ddata/$domain.data`
echo 'DROP DATABASE '`awk '{print $2}' /etc/nginx/ddata/$domain.data`';' | /usr/local/mysql/bin/mysql -u root -p`awk '{print $3}' /etc/nginx/ddata/$domain.data`
echo "FLUSH PRIVILEGES;" | /usr/local/mysql/bin/mysql -u root -p`awk '{print $3}' /etc/nginx/ddata/$domain.data`
fi
rm -rf /srv/$webdrive/$domain /etc/nginx/sites-available/$domain /etc/nginx/sites-enabled/$domain /etc/nginx/ddata/$domain.data
/etc/init.d/nginx restart
echo ""
echo "Your domain has been removed!"
echo ""
fi
